//
//  LiveLightningServerTests.m
//  LiveLightningServerTests
//
//  Created by Dominic Ong on 6/12/14.
//  Copyright (c) 2014 Dom Ong. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface LiveLightningServerTests : XCTestCase

@end

@implementation LiveLightningServerTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
