//
//  LiveLightningAppDelegate.h
//  LiveLightningServer
//
//  Created by Dominic Ong on 6/12/14.
//  Copyright (c) 2014 Dom Ong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveLightningAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
