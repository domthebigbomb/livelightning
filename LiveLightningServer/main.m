//
//  main.m
//  LiveLightningServer
//
//  Created by Dominic Ong on 6/12/14.
//  Copyright (c) 2014 Dom Ong. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LiveLightningAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LiveLightningAppDelegate class]));
    }
}
