//
//  LiveLightningViewController.m
//  LiveLightningServer
//
//  Created by Dominic Ong on 6/12/14.
//  Copyright (c) 2014 Dom Ong. All rights reserved.
//

#import "LiveLightningViewController.h"

@interface LiveLightningViewController ()
@property (strong,nonatomic) IBOutlet MKMapView *mapview;
-(IBAction)addLightningAnnotation:(UIButton *)sender;
@end

@implementation LiveLightningViewController{
    SRConnection *connection;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_mapview setDelegate:self];
    

    [self openConnection];
}

-(void)viewWillAppear:(BOOL)animated{
    [connection start];
}

-(void)viewWillDisappear:(BOOL)animated{
    [connection stop];
}

-(void)openConnection{
    NSString *serviceURLString = @"http://qa-livelx-845878501.us-east-1.elb.amazonaws.com/LiveSpark/livespark";
    connection = [SRConnection connectionWithURL:serviceURLString];
    
    NSNumber *north, *south, *east, *west;

    /*
     north = [NSNumber numberWithDouble:90];
     south = [NSNumber numberWithDouble:-90];
     east = [NSNumber numberWithDouble:180];
     west = [NSNumber numberWithDouble:-180];
     */
    
    north = [NSNumber numberWithDouble:49.59];
    south = [NSNumber numberWithDouble:24.94];
    east = [NSNumber numberWithDouble:-66.93];
    west = [NSNumber numberWithDouble:-125];
    
    /*
     north = [NSNumber numberWithDouble:50];
     south = [NSNumber numberWithDouble:0];
     east = [NSNumber numberWithDouble:-99];
     west = [NSNumber numberWithDouble:-103];
     */
    
    CLLocationDegrees lat = ([north doubleValue]-[south doubleValue])/2 + [south doubleValue];
    CLLocationDegrees lon = ([east doubleValue]-[west doubleValue])/2 + [west doubleValue];
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(lat, lon);
    MKCoordinateSpan span = MKCoordinateSpanMake([north doubleValue]-[south doubleValue], [east doubleValue]-[west doubleValue]);
    MKCoordinateRegion zoomedRegion = [_mapview regionThatFits:MKCoordinateRegionMake(center, span)];
    [_mapview setRegion: zoomedRegion];
    
    NSString *authid = @"";
    NSUInteger timestamp = [[NSString stringWithFormat:@"%f",[[NSDate date]timeIntervalSince1970] * 1000] integerValue];
    NSLog(@"timestamp:%lu",(unsigned long)timestamp);
    
    // Calculating hmac
    NSString *requestString = [NSString stringWithFormat:@"%d|%d|%d|%d|%lu",(int)floor(100*(300+[north doubleValue])),(int)floor(100*(300+[south doubleValue])),(int)floor(100*(300+[east doubleValue])),(int)floor(100*(300+[west doubleValue])),(unsigned long)timestamp];
    NSLog(@"Request String:%@",requestString);
    
    NSLog(@"hmac:%@",requestString);
    
    connection.started = ^{
        NSLog(@"Connection started");
        NSError *error;
        NSDictionary *clientSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        north, @"north",
                                        south, @"south",
                                        east, @"east",
                                        west, @"west",
                                        [NSNumber numberWithInt:0],@"secondsToRefresh",
                                        [NSNumber numberWithUnsignedInteger:timestamp], @"timestamp",
                                        @"Request String",@"hmac",
                                        authid, @"authid",
                                        nil];
        NSLog(@"ClientSettings:%@",[clientSettings description]);
        NSData *serializedSettings = [NSJSONSerialization dataWithJSONObject:clientSettings options:NSJSONWritingPrettyPrinted error:&error];
        NSString *serializedString = [[NSString alloc] initWithData:serializedSettings encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized String:%@",serializedString);
        [connection send:serializedString];
    };
    
    connection.received = ^(NSDictionary *data){
        if(data != nil){
            NSDictionary *response = [[NSDictionary alloc] initWithDictionary:data];
            NSLog(@"%lu",(unsigned long)[[response objectForKey:@"r"] count]);
            BOOL singlePixel = ([[response objectForKey:@"r"] count] == 4) ? YES : NO;
            NSLog(@"Single Pixel: %d",singlePixel);
            NSLog(@"Data response:%@",[response description]);
            if(singlePixel){
                [self addLightning:[response objectForKey:@"r"]];
            }else{
                NSLog(@"Multiple Pixels Found");
                NSDictionary *result = [[NSDictionary alloc] initWithDictionary: [response objectForKey:@"r"]];
                NSArray *pixels = [[NSArray alloc] initWithArray:[result objectForKey:@"p"]];
                for(NSDictionary *pixel in pixels){
                    [self addLightning:pixel];
                }
            }
        }
    };
    //[connection start];
}

-(void)addLightning:(NSDictionary *)lightningPixel{
    NSLog(@"Lightning Pixel added: %@",[lightningPixel description]);
    CLLocationDegrees lat = [[lightningPixel objectForKey:@"la"] doubleValue];
    CLLocationDegrees lon = [[lightningPixel objectForKey:@"lo"] doubleValue];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat, lon);
    MKPointAnnotation *placemark = [[MKPointAnnotation alloc] init];
    [placemark setCoordinate:coordinate];
    [_mapview addAnnotation:placemark];
    dispatch_async(dispatch_get_main_queue(), ^{
        MKAnnotationView *view = [_mapview viewForAnnotation:placemark];
        //if(view){
        //[view setImage:[UIImage imageWithContentsOfFile:@"lightning.png"]];
            view.alpha = 0.0;
            [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                view.alpha = 1.0;
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:6.0 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    view.alpha = 0.0;
                } completion:^(BOOL finished) {
                    [_mapview removeAnnotation:placemark];
                }];
            }];
    });
}



-(void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{
    NSLog(@"Region Will Change");
    [connection stop];
}

-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    NSLog(@"Regional Did Change");
    
    CGPoint NEpt = CGPointMake( _mapview.bounds.origin.x + _mapview.bounds.size.width, _mapview.bounds.origin.y);
    CGPoint SWpt = CGPointMake( _mapview.bounds.origin.x , _mapview.bounds.origin.y + _mapview.bounds.size.height);
    CLLocationCoordinate2D NEcoordinate = [_mapview convertPoint:NEpt toCoordinateFromView:_mapview];
    CLLocationCoordinate2D SWcoordinate = [_mapview convertPoint:SWpt toCoordinateFromView:_mapview];
    
    
    
    NSNumber *north, *south, *east, *west;
    
    north = [NSNumber numberWithDouble:(double) NEcoordinate.latitude];
    south = [NSNumber numberWithDouble:(double) SWcoordinate.latitude];
    east = [NSNumber numberWithDouble:(double) NEcoordinate.longitude];
    west = [NSNumber numberWithDouble:(double) SWcoordinate.longitude];
    
    NSLog(@"North: %@", north);
    NSLog(@"South: %@", south);
    NSLog(@"East: %@", east);
    NSLog(@"West: %@", west);

    
    NSUInteger timestamp = [[NSString stringWithFormat:@"%f",[[NSDate date]timeIntervalSince1970] * 1000] integerValue];
    
    connection.started = ^{
        NSLog(@"Connection started");
        NSError *error;
        NSDictionary *clientSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        north, @"north",
                                        south, @"south",
                                        east, @"east",
                                        west, @"west",
                                        [NSNumber numberWithInt:0],@"secondsToRefresh",
                                        [NSNumber numberWithUnsignedInteger:timestamp], @"timestamp",
                                        @"Request Strings",@"hmac",
                                        @"Auth ID", @"authid",
                                        nil];
        NSLog(@"ClientSettings:%@",[clientSettings description]);
        NSData *serializedSettings = [NSJSONSerialization dataWithJSONObject:clientSettings options:NSJSONWritingPrettyPrinted error:&error];
        NSString *serializedString = [[NSString alloc] initWithData:serializedSettings encoding:NSUTF8StringEncoding];
        NSLog(@"Serialized String:%@",serializedString);
        [connection send:serializedString];
    };
    [connection start];
}

-(void)removeLightningPixel:(NSTimer *)timer{
    [_mapview removeAnnotation:[timer userInfo]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}


-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    MKAnnotationView *view = [[MKAnnotationView alloc] init];
    view.image = [UIImage imageNamed:@"lightning"];
    CGRect rect = CGRectMake(view.frame.origin.x, view.frame.origin.y, 24, 25);
    [view setFrame: rect];
    return view;
}

-(IBAction)addLightningAnnotation:(UIButton *)sender{
   
}

@end
