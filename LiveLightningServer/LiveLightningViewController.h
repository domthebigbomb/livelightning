//
//  LiveLightningViewController.h
//  LiveLightningServer
//
//  Created by Dominic Ong on 6/12/14.
//  Copyright (c) 2014 Dom Ong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SignalR.h>
#import <MapKit/MapKit.h>
#import <CommonCrypto/CommonCrypto.h>

@interface LiveLightningViewController : UIViewController<MKMapViewDelegate>

@end
